const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let find = true;
    if (await page.$('body > div > form > div:nth-child(4) > button') !== null) console.log('found');
    else find = false;
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    expect(find).toBe(true);
    await browser.close();

})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    //await page.waitForNavigation();
    await page.waitFor(1500);
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();

})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.$eval( 'body > div > form > div:nth-child(4) > button', form => form.click() );
    await page.waitFor(1500)
    await page.screenshot({path: 'test/screenshots/err.png'});
    await browser.close();

})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(3000);
    // test
    let message = await page.$eval('.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Hi John, Welcome to the chat app');

    await browser.close();

})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');

    await browser.close();

})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(3000);
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    await browser.close()
   
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(3000);
    // user2
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();

    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page2.waitFor(3000);

    let member1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member1).toBe('John');
    expect(member2).toBe('Mike');

    await browser.close();
    await browser2.close();


},30000)

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(3000);
    let title = await page.$eval('body > div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    let find = true;
    if (await page.$('body > div:nth-child(2) > div > form > button') !== null) console.log('found');
    else find = false;
    expect(title).toBe('Send');
    expect(find).toBe(true);
    await browser.close();

})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);
    
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(3000);
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Testing');
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(1000);


    let sender = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    expect(sender).toBe("John");
    expect(message).toBe("Testing");
    await browser.close();	
},20000)

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    //login/////
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    // user2
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    
    //send message
    await page.waitFor(2000);
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hi');
    await page.keyboard.press('Enter', {delay: 100});

    await page.waitFor(1000);

    await page2.waitFor(2000);
    await page2.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hello');
    await page2.keyboard.press('Enter', {delay: 100});

    await page2.waitFor(1000);

    //Testing
    let sender1 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
    });
    let message1 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    let sender2 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(1) > h4').innerHTML;
    });
    let message2 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });
    let sender3 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    let message3 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });    
    let sender4 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
    });
    let message4 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    
    expect(sender1).toBe('John');
    expect(message1).toBe('Hi');
    expect(sender2).toBe('Mike');
    expect(message2).toBe('Hello');
    expect(sender3).toBe('John');
    expect(message3).toBe('Hi');
    expect(sender4).toBe('Mike');
    expect(message4).toBe('Hello');


    await browser.close();
    await browser2.close();


},60000)

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(3000);
    let title =  await page.$eval('body > div:nth-child(2) > div > button', (content) => content.innerHTML);
    let find = true;
    if (await page.$('body > div:nth-child(2) > div >  button') !== null) console.log('found');
    else find = false;
    expect(title).toBe('Send location');
    expect(find).toBe(true);
    await browser.close();

})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);


    const context = browser.defaultBrowserContext();
    await context.overridePermissions('http://localhost:3000/', ['geolocation']);
    await page.setGeolocation({latitude: 25.021644799999997, longitude: 121.5463424});


    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(3000);


    await page.click('#send-location');
    await page.waitFor(5000);
    await page.screenshot({path: 'test/screenshots/Location.png'});
    // test
    // expect to sent a message of maps.google.com API which shows the map
    let messageBody = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > div > iframe').src;
    });
    api = new URL(messageBody);
    expect(api.hostname).toBe('maps.google.com');

    await browser.close();
}, 30000);

